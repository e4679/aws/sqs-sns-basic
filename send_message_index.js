const {createUser} = require('./service/users');

const user = {
  email: 'maria@gmail.com',
  lastName: 'Casas',
  firstName: 'Maria',
  position: '5'
};

createUser(user)
  .then((response) => {
    console.log(response);
  })
  .catch((err) => {
    console.log(err)
  })
