const {publishMessage} = require('./helpers/aws/sns');
const config = require('./config');

const user = {Email: 'emeneses@gmail.com'};

publishMessage(JSON.stringify(user), config.sns.testTopicARN)
  .then((response) => {
    console.log(response);
  })
  .catch((err) => {
    console.log(err);
  });
