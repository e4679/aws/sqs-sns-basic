module.exports = {
  sqs: {
    url: process.env.SQS_URL,
  },
  sns: {
    testTopicARN: process.env.SNS_ARN,
  },
}
