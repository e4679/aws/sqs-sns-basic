const {sendMessage, readMessage} = require('../helpers/aws/sqs');
const userAttributes = require('../schemas/user_attributes_string');
const config = require('../config');

exports.createUser = (user) =>
  sendMessage(
    userAttributes(user),
    config.sqs.url,
    JSON.stringify(user)
  );

exports.readUser = () =>
  readMessage(config.sqs.url)
    .then((response) => {
      const messages = response && response.Messages;
      if(messages) {
        return messages[0]
      }else {
        return 'Not message available';
      }
    });

