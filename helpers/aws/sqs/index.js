const AWS = require('../../aws');

const sqs = new AWS.SQS();

exports.sendMessage = (attributes, url, message) => {
  const params = {
    DelaySeconds: 10,
    MessageAttributes: {...attributes},
    MessageBody: message,
    QueueUrl: url
  };
  return sqs.sendMessage(params).promise();
};

exports.readMessage = (url) => {
  const params = {
    MaxNumberOfMessages: 10,
    MessageAttributeNames: ['All'],
    QueueUrl: url,
    VisibilityTimeout: 20,
    WaitTimeSeconds: 20,
  }
  return sqs.receiveMessage(params).promise();
}
