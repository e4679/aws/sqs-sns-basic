const AWS = require('../../aws');

const sns = new AWS.SNS();

exports.publishMessage = (message, snsARN) => {
  const params = {
    Message: message,
    MessageAttributes: {
      Email: {
        DataType: 'String',
        StringValue: 'edgar.a.meneses@accenture.com'
      }
    },
    TopicArn: snsARN
  };
  return sns.publish(params).promise();
}
