module.exports = (userData) => ({
  Email: {
    DataType: 'String',
    StringValue: userData.email,
  },
  LastName: {
    DataType: 'String',
    StringValue: userData.lastName,
  },
  FirstName: {
    DataType: 'String',
    StringValue: userData.firstName,
  },
  Position: {
    DataType: 'Number',
    StringValue: userData.position,
  },
})
