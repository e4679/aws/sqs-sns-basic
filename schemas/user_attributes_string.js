module.exports = (userData) => ({
  'Data': {
    DataType: 'String',
    StringValue: JSON.stringify(userData),
  },
});
